﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ionic.Zip;
using System.Diagnostics;
using System.Xml.Linq;
using System.Web;
using System.Net;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        string download = "";
        string kspVersion = "";

        public Form1()
        {
            InitializeComponent();

            if (File.Exists("kspfolder.db"))
            {
                System.IO.StreamReader file = new System.IO.StreamReader("kspfolder.db");
                textBoxKspDir.Text = file.ReadLine();
                file.Close();
                textBoxKspDir.ForeColor = Color.Green;
                Settings.correctDir = "true";
                Settings.kspDir = textBoxKspDir.Text;
                checkKspVersion();
                labelSettingsVersion.Text = "Your version number is " + kspVersion;
            }

            Settings.xmlPath = "http://minimalistiskdesign.nu/ksp/items.xml";

            if (Settings.correctDir == "true")
            {
                enableInstalledAddons();
            }
            else
            {
                listBoxInstalled.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog kspDir = new FolderBrowserDialog();
            kspDir.ShowDialog();
            textBoxKspDir.Text = kspDir.SelectedPath;
            if (File.Exists(kspDir.SelectedPath + "/KSP_Data/sharedassets1.assets"))
            {
                textBoxKspDir.ForeColor = Color.Green;
                //START STORAGE
                System.IO.StreamWriter writer;
                writer = System.IO.File.CreateText("kspfolder.db");
                writer.WriteLine(kspDir.SelectedPath);
                writer.Close();
                //END STORAGE
                checkKspVersion();
                labelSettingsVersion.Text = "Your version number is "+kspVersion;

                Settings.correctDir = "true";
                Settings.kspDir = kspDir.SelectedPath;
                enableInstalledAddons();
            }
            else
            {
                textBoxKspDir.ForeColor = Color.Red;
                disableInstalledAddons();
            }
            
        }

        public void enableInstalledAddons() {
            labelInstalled.Visible = false;
            listBoxInstalled.Visible = true;
            buttonInformation.Enabled = true;
            buttonRemove.Enabled = true;
            buttonUpdate.Enabled = true;
            buttonInstall.Enabled = true;
            buttonImport.Enabled = true;

            updateInstalledAddons();
        }
        public void disableInstalledAddons()
        {
            labelInstalled.Visible = true;
            listBoxInstalled.Visible = false;
            buttonInformation.Enabled = false;
            buttonRemove.Enabled = false;
            buttonUpdate.Enabled = false;
            buttonInstall.Enabled = false;
            buttonImport.Enabled = false;

            updateInstalledAddons();
        }

        public void updateInstalledAddons()
        {
            listBoxInstalled.Items.Clear();
            foreach (string addon in System.IO.Directory.GetDirectories(Settings.kspDir + @"\Parts"))
            {
                string addonFilename = addon + @"\part.cfg";
                listBoxInstalled.Items.Add(addon);
            }
        }

        public void checkKspVersion()
        {
            //START VERSION CHECK
            string[] fileContents = File.ReadAllLines(Settings.kspDir + "/readme.txt");

            // assume this is user input and not hardcoded
            int startingLine = 16;
            int endingLine = 16;

            var query = fileContents.Skip(startingLine - 1).Take(endingLine - (startingLine - 1));

            foreach (string line in query)
                kspVersion = line.Substring(8);

            //MessageBox.Show(kspVersion);
            //STOP VERSION CHECK
        }

        public void openLink(string link)
        {
            System.Diagnostics.Process.Start(link);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            string folder = Convert.ToString(listBoxInstalled.SelectedItem);
            System.IO.Directory.Delete(folder, true);
            updateInstalledAddons();
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This action will take some time depending on the amount of addons. Please wait and don't exit the application while making a backup.");
            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(Settings.kspDir, "Parts");
                saveFileDialog1.ShowDialog();
                string saveLocation = saveFileDialog1.FileName;
                zip.Save(saveLocation);
            }
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string extension = Convert.ToString(Path.GetExtension(openFileDialog1.FileName));
            if (extension == ".zip")
            {
                string zipToUnpack = openFileDialog1.FileName;
                string unpackDirectory = Settings.kspDir + "/Parts/";
                using (ZipFile zip1 = ZipFile.Read(zipToUnpack))
                {
                    // here, we extract every entry, but we could extract conditionally
                    // based on entry name, size, date, checkbox status, etc.  
                    foreach (ZipEntry file in zip1)
                    {
                        file.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                    }
                }
                updateInstalledAddons();
            }
            else
            {
                MessageBox.Show("Wrong extension");
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            updateInstalledAddons();
        }

        private void buttonInformation_Click(object sender, EventArgs e)
        {
            string selectedItem = Convert.ToString(listBoxInstalled.SelectedItem)+@"\part.cfg";
            //MessageBox.Show(selectedItem);
            ProcessStartInfo startInfo = new ProcessStartInfo("notepad.exe", selectedItem);
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            richTextBoxDesc.BackColor = this.BackColor;
            XDocument xmlDoc = XDocument.Load(Settings.xmlPath);
            var q = from c in xmlDoc.Descendants("part")
                    //Getting value: (string)c.Element("url")
                    select (string)c.Element("name");
            foreach (string name in q)
            {
                listBoxAddons.Items.Add(name);
            }
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxAddons.Items.Clear();
            if (Convert.ToString(comboBox1.SelectedItem) == "All")
            {
                XDocument xmlDoc = XDocument.Load(Settings.xmlPath);
                var q = from c in xmlDoc.Descendants("part")
                        //Getting value: (string)c.Element("url")
                        select (string)c.Element("name");
                foreach (string name in q)
                {
                    listBoxAddons.Items.Add(name);
                }
            }
            else
            {
                XDocument xmlDoc = XDocument.Load(Settings.xmlPath);
                var q = from c in xmlDoc.Descendants("part")
                        where c.Element("category").Value == Convert.ToString(comboBox1.SelectedItem)
                        //Getting value: (string)c.Element("url")
                        select (string)c.Element("name");
                foreach (string name in q)
                {
                    listBoxAddons.Items.Add(name);
                }
            }
        }

        private void listBoxAddons_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedItem = Convert.ToString(listBoxAddons.SelectedItem);
            XDocument xmlDoc = XDocument.Load(Settings.xmlPath);
            var q = from c in xmlDoc.Descendants("part")
                    where c.Element("name").Value == selectedItem
                    select new
                    {
                        name = c.Element("name").Value,
                        category = c.Element("category").Value,
                        desc = c.Element("desc").Value,
                        manu = c.Element("manu").Value,
                        author = c.Element("author").Value,
                        version = c.Element("version").Value,
                        imageLocation = c.Element("image").Value,
                        download = c.Element(@"download").Value
                    };
            foreach (var obj in q)
            {

                labelName.Text = obj.name;
                labelCategory.Text = obj.category;
                richTextBoxDesc.Text = obj.desc;
                labelManu.Text = obj.manu;
                labelAuthor.Text = obj.author;
                if (obj.version == "unknown")
                {
                    labelVersion.ForeColor = Color.Red;
                }
                else
                {
                    if (obj.version == kspVersion)
                    {
                        labelVersion.ForeColor = Color.Green;
                    }
                    else
                    {
                        labelVersion.ForeColor = Color.Red;
                    }
                    
                }
                labelVersion.Text = "For version "+obj.version;
                pictureBox1.ImageLocation = obj.imageLocation;
                download = obj.download;
            }
        }

        private void buttonInstall_Click(object sender, EventArgs e)
        {
            buttonInstall.Enabled = false;
            DownloadAddon(download);
        }

        public void DownloadAddon(string download)
        {
            //MessageBox.Show(download);
            WebClient Client = new WebClient();
            Client.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadComplete);
            Client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

            Client.DownloadFileAsync(new Uri(download), "download.zip");
        }
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void DownloadComplete(object sender, AsyncCompletedEventArgs e)
        {
            string zipToUnpack = "download.zip";
            string unpackDirectory = Settings.kspDir + "/Parts/";
            using (ZipFile zip1 = ZipFile.Read(zipToUnpack))
            {
                // here, we extract every entry, but we could extract conditionally
                // based on entry name, size, date, checkbox status, etc.  
                foreach (ZipEntry file in zip1)
                {
                    file.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                }
            }
            buttonInstall.Enabled = true;
            updateInstalledAddons();
            MessageBox.Show("Addon installed successfully");
            progressBar1.Value = 0;
        }

        private void richTextBoxDesc_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText); 
        }
    }
    static class Settings
    {
        private static string v_correctDir = "";
        private static string v_kspDir = "";
        private static string v_xmlPath = "";

        public static string correctDir
        {
            get { return v_correctDir; }
            set { v_correctDir = value; }
        }

        public static string kspDir
        {
            get { return v_kspDir; }
            set { v_kspDir = value; }
        }

        public static string xmlPath
        {
            get { return v_xmlPath; }
            set { v_xmlPath = value; }
        }


    }
}