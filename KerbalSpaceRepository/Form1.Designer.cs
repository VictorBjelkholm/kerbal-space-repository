﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabHome = new System.Windows.Forms.TabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tabAddons = new System.Windows.Forms.TabPage();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonInstall = new System.Windows.Forms.Button();
            this.groupBoxInformation = new System.Windows.Forms.GroupBox();
            this.richTextBoxDesc = new System.Windows.Forms.RichTextBox();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelCategory = new System.Windows.Forms.Label();
            this.buttonForCategory = new System.Windows.Forms.Label();
            this.labelAuthor = new System.Windows.Forms.Label();
            this.labelForAuthor = new System.Windows.Forms.Label();
            this.labelManu = new System.Windows.Forms.Label();
            this.labelForManu = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelForDescription = new System.Windows.Forms.Label();
            this.labelForName = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.listBoxAddons = new System.Windows.Forms.ListBox();
            this.tabInstalled = new System.Windows.Forms.TabPage();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonBackup = new System.Windows.Forms.Button();
            this.buttonImport = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonInformation = new System.Windows.Forms.Button();
            this.labelInstalled = new System.Windows.Forms.Label();
            this.listBoxInstalled = new System.Windows.Forms.ListBox();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.labelSettingsVersion = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonKspDir = new System.Windows.Forms.Button();
            this.textBoxKspDir = new System.Windows.Forms.TextBox();
            this.labelKspDir = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabHome.SuspendLayout();
            this.tabAddons.SuspendLayout();
            this.groupBoxInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabInstalled.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabHome);
            this.tabControl1.Controls.Add(this.tabAddons);
            this.tabControl1.Controls.Add(this.tabInstalled);
            this.tabControl1.Controls.Add(this.tabSettings);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(600, 438);
            this.tabControl1.TabIndex = 0;
            // 
            // tabHome
            // 
            this.tabHome.Controls.Add(this.webBrowser1);
            this.tabHome.Location = new System.Drawing.Point(4, 22);
            this.tabHome.Name = "tabHome";
            this.tabHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabHome.Size = new System.Drawing.Size(592, 412);
            this.tabHome.TabIndex = 0;
            this.tabHome.Text = "Home";
            this.tabHome.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(3, 3);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(586, 406);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.Url = new System.Uri("http://minimalistiskdesign.nu/ksp/home.html", System.UriKind.Absolute);
            // 
            // tabAddons
            // 
            this.tabAddons.Controls.Add(this.progressBar1);
            this.tabAddons.Controls.Add(this.buttonInstall);
            this.tabAddons.Controls.Add(this.groupBoxInformation);
            this.tabAddons.Controls.Add(this.comboBox1);
            this.tabAddons.Controls.Add(this.pictureBox1);
            this.tabAddons.Controls.Add(this.listBoxAddons);
            this.tabAddons.Location = new System.Drawing.Point(4, 22);
            this.tabAddons.Name = "tabAddons";
            this.tabAddons.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddons.Size = new System.Drawing.Size(592, 412);
            this.tabAddons.TabIndex = 1;
            this.tabAddons.Text = "Addons";
            this.tabAddons.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(196, 378);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(303, 23);
            this.progressBar1.TabIndex = 5;
            // 
            // buttonInstall
            // 
            this.buttonInstall.Enabled = false;
            this.buttonInstall.Location = new System.Drawing.Point(504, 378);
            this.buttonInstall.Name = "buttonInstall";
            this.buttonInstall.Size = new System.Drawing.Size(75, 23);
            this.buttonInstall.TabIndex = 4;
            this.buttonInstall.Text = "Install";
            this.buttonInstall.UseVisualStyleBackColor = true;
            this.buttonInstall.Click += new System.EventHandler(this.buttonInstall_Click);
            // 
            // groupBoxInformation
            // 
            this.groupBoxInformation.Controls.Add(this.richTextBoxDesc);
            this.groupBoxInformation.Controls.Add(this.labelVersion);
            this.groupBoxInformation.Controls.Add(this.labelCategory);
            this.groupBoxInformation.Controls.Add(this.buttonForCategory);
            this.groupBoxInformation.Controls.Add(this.labelAuthor);
            this.groupBoxInformation.Controls.Add(this.labelForAuthor);
            this.groupBoxInformation.Controls.Add(this.labelManu);
            this.groupBoxInformation.Controls.Add(this.labelForManu);
            this.groupBoxInformation.Controls.Add(this.labelName);
            this.groupBoxInformation.Controls.Add(this.labelForDescription);
            this.groupBoxInformation.Controls.Add(this.labelForName);
            this.groupBoxInformation.Location = new System.Drawing.Point(187, 172);
            this.groupBoxInformation.Name = "groupBoxInformation";
            this.groupBoxInformation.Size = new System.Drawing.Size(399, 200);
            this.groupBoxInformation.TabIndex = 3;
            this.groupBoxInformation.TabStop = false;
            this.groupBoxInformation.Text = "Information";
            // 
            // richTextBoxDesc
            // 
            this.richTextBoxDesc.Location = new System.Drawing.Point(9, 48);
            this.richTextBoxDesc.Name = "richTextBoxDesc";
            this.richTextBoxDesc.ReadOnly = true;
            this.richTextBoxDesc.Size = new System.Drawing.Size(383, 79);
            this.richTextBoxDesc.TabIndex = 21;
            this.richTextBoxDesc.Text = "";
            this.richTextBoxDesc.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTextBoxDesc_LinkClicked);
            // 
            // labelVersion
            // 
            this.labelVersion.Location = new System.Drawing.Point(264, 174);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(128, 13);
            this.labelVersion.TabIndex = 20;
            this.labelVersion.Text = "For version";
            // 
            // labelCategory
            // 
            this.labelCategory.Location = new System.Drawing.Point(72, 174);
            this.labelCategory.Name = "labelCategory";
            this.labelCategory.Size = new System.Drawing.Size(320, 13);
            this.labelCategory.TabIndex = 19;
            this.labelCategory.Text = "Load a addon";
            // 
            // buttonForCategory
            // 
            this.buttonForCategory.AutoSize = true;
            this.buttonForCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonForCategory.Location = new System.Drawing.Point(5, 174);
            this.buttonForCategory.Name = "buttonForCategory";
            this.buttonForCategory.Size = new System.Drawing.Size(61, 13);
            this.buttonForCategory.TabIndex = 18;
            this.buttonForCategory.Text = "Category:";
            // 
            // labelAuthor
            // 
            this.labelAuthor.Location = new System.Drawing.Point(59, 152);
            this.labelAuthor.Name = "labelAuthor";
            this.labelAuthor.Size = new System.Drawing.Size(333, 13);
            this.labelAuthor.TabIndex = 17;
            this.labelAuthor.Text = "Load a addon";
            // 
            // labelForAuthor
            // 
            this.labelForAuthor.AutoSize = true;
            this.labelForAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelForAuthor.Location = new System.Drawing.Point(5, 152);
            this.labelForAuthor.Name = "labelForAuthor";
            this.labelForAuthor.Size = new System.Drawing.Size(48, 13);
            this.labelForAuthor.TabIndex = 16;
            this.labelForAuthor.Text = "Author:";
            // 
            // labelManu
            // 
            this.labelManu.Location = new System.Drawing.Point(97, 130);
            this.labelManu.Name = "labelManu";
            this.labelManu.Size = new System.Drawing.Size(295, 13);
            this.labelManu.TabIndex = 15;
            this.labelManu.Text = "Load a addon";
            // 
            // labelForManu
            // 
            this.labelForManu.AutoSize = true;
            this.labelForManu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelForManu.Location = new System.Drawing.Point(5, 130);
            this.labelForManu.Name = "labelForManu";
            this.labelForManu.Size = new System.Drawing.Size(86, 13);
            this.labelForManu.TabIndex = 14;
            this.labelForManu.Text = "Manufacturer:";
            // 
            // labelName
            // 
            this.labelName.Location = new System.Drawing.Point(59, 16);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(334, 16);
            this.labelName.TabIndex = 13;
            this.labelName.Text = "Load a addon";
            // 
            // labelForDescription
            // 
            this.labelForDescription.AutoSize = true;
            this.labelForDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelForDescription.Location = new System.Drawing.Point(6, 32);
            this.labelForDescription.Name = "labelForDescription";
            this.labelForDescription.Size = new System.Drawing.Size(71, 13);
            this.labelForDescription.TabIndex = 11;
            this.labelForDescription.Text = "Description";
            // 
            // labelForName
            // 
            this.labelForName.AutoSize = true;
            this.labelForName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelForName.Location = new System.Drawing.Point(6, 16);
            this.labelForName.Name = "labelForName";
            this.labelForName.Size = new System.Drawing.Size(47, 13);
            this.labelForName.TabIndex = 10;
            this.labelForName.Text = "Name: ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "All",
            "Propulsion",
            "Command and Control",
            "Structural and Aerodynamic",
            "Utility and Scientific",
            "Decals",
            "Crew",
            "Other/Packs"});
            this.comboBox1.Location = new System.Drawing.Point(7, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(173, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.Text = "All";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(187, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 150);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // listBoxAddons
            // 
            this.listBoxAddons.FormattingEnabled = true;
            this.listBoxAddons.Location = new System.Drawing.Point(7, 34);
            this.listBoxAddons.Name = "listBoxAddons";
            this.listBoxAddons.Size = new System.Drawing.Size(173, 368);
            this.listBoxAddons.TabIndex = 0;
            this.listBoxAddons.SelectedIndexChanged += new System.EventHandler(this.listBoxAddons_SelectedIndexChanged);
            // 
            // tabInstalled
            // 
            this.tabInstalled.Controls.Add(this.buttonUpdate);
            this.tabInstalled.Controls.Add(this.buttonBackup);
            this.tabInstalled.Controls.Add(this.buttonImport);
            this.tabInstalled.Controls.Add(this.buttonRemove);
            this.tabInstalled.Controls.Add(this.buttonInformation);
            this.tabInstalled.Controls.Add(this.labelInstalled);
            this.tabInstalled.Controls.Add(this.listBoxInstalled);
            this.tabInstalled.Location = new System.Drawing.Point(4, 22);
            this.tabInstalled.Name = "tabInstalled";
            this.tabInstalled.Size = new System.Drawing.Size(592, 412);
            this.tabInstalled.TabIndex = 3;
            this.tabInstalled.Text = "Installed addons";
            this.tabInstalled.UseVisualStyleBackColor = true;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Enabled = false;
            this.buttonUpdate.Location = new System.Drawing.Point(259, 386);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 6;
            this.buttonUpdate.Text = "Update list";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonBackup
            // 
            this.buttonBackup.Enabled = false;
            this.buttonBackup.Location = new System.Drawing.Point(432, 386);
            this.buttonBackup.Name = "buttonBackup";
            this.buttonBackup.Size = new System.Drawing.Size(75, 23);
            this.buttonBackup.TabIndex = 5;
            this.buttonBackup.Text = "Backup";
            this.buttonBackup.UseVisualStyleBackColor = true;
            this.buttonBackup.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // buttonImport
            // 
            this.buttonImport.Enabled = false;
            this.buttonImport.Location = new System.Drawing.Point(513, 386);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(75, 23);
            this.buttonImport.TabIndex = 4;
            this.buttonImport.Text = "Import";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Enabled = false;
            this.buttonRemove.Location = new System.Drawing.Point(84, 386);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(75, 23);
            this.buttonRemove.TabIndex = 3;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonInformation
            // 
            this.buttonInformation.Enabled = false;
            this.buttonInformation.Location = new System.Drawing.Point(3, 386);
            this.buttonInformation.Name = "buttonInformation";
            this.buttonInformation.Size = new System.Drawing.Size(75, 23);
            this.buttonInformation.TabIndex = 2;
            this.buttonInformation.Text = "Information";
            this.buttonInformation.UseVisualStyleBackColor = true;
            this.buttonInformation.Click += new System.EventHandler(this.buttonInformation_Click);
            // 
            // labelInstalled
            // 
            this.labelInstalled.AutoSize = true;
            this.labelInstalled.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInstalled.Location = new System.Drawing.Point(58, 169);
            this.labelInstalled.Name = "labelInstalled";
            this.labelInstalled.Size = new System.Drawing.Size(476, 74);
            this.labelInstalled.TabIndex = 0;
            this.labelInstalled.Text = "This page is only available if you\r\ngot the right directory selected";
            this.labelInstalled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxInstalled
            // 
            this.listBoxInstalled.FormattingEnabled = true;
            this.listBoxInstalled.Location = new System.Drawing.Point(3, 3);
            this.listBoxInstalled.Name = "listBoxInstalled";
            this.listBoxInstalled.Size = new System.Drawing.Size(585, 381);
            this.listBoxInstalled.TabIndex = 1;
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.labelSettingsVersion);
            this.tabSettings.Controls.Add(this.label1);
            this.tabSettings.Controls.Add(this.buttonKspDir);
            this.tabSettings.Controls.Add(this.textBoxKspDir);
            this.tabSettings.Controls.Add(this.labelKspDir);
            this.tabSettings.Location = new System.Drawing.Point(4, 22);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Size = new System.Drawing.Size(592, 412);
            this.tabSettings.TabIndex = 2;
            this.tabSettings.Text = "Settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // labelSettingsVersion
            // 
            this.labelSettingsVersion.Location = new System.Drawing.Point(8, 54);
            this.labelSettingsVersion.Name = "labelSettingsVersion";
            this.labelSettingsVersion.Size = new System.Drawing.Size(501, 23);
            this.labelSettingsVersion.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(119, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(250, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "If text is green, you\'ve selected the correct directory";
            // 
            // buttonKspDir
            // 
            this.buttonKspDir.Location = new System.Drawing.Point(515, 28);
            this.buttonKspDir.Name = "buttonKspDir";
            this.buttonKspDir.Size = new System.Drawing.Size(75, 23);
            this.buttonKspDir.TabIndex = 2;
            this.buttonKspDir.Text = "Browse";
            this.buttonKspDir.UseVisualStyleBackColor = true;
            this.buttonKspDir.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxKspDir
            // 
            this.textBoxKspDir.ForeColor = System.Drawing.Color.Red;
            this.textBoxKspDir.Location = new System.Drawing.Point(8, 31);
            this.textBoxKspDir.Name = "textBoxKspDir";
            this.textBoxKspDir.Size = new System.Drawing.Size(501, 20);
            this.textBoxKspDir.TabIndex = 1;
            // 
            // labelKspDir
            // 
            this.labelKspDir.AutoSize = true;
            this.labelKspDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKspDir.Location = new System.Drawing.Point(4, 4);
            this.labelKspDir.Name = "labelKspDir";
            this.labelKspDir.Size = new System.Drawing.Size(108, 24);
            this.labelKspDir.TabIndex = 0;
            this.labelKspDir.Text = "KSP folder";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "ZIP Archives|*.zip";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "Backup_of_addons.zip";
            this.saveFileDialog1.Filter = "ZIP Archives|*.zip";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 462);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Kerbal Space Repository Beta 0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabHome.ResumeLayout(false);
            this.tabAddons.ResumeLayout(false);
            this.groupBoxInformation.ResumeLayout(false);
            this.groupBoxInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabInstalled.ResumeLayout(false);
            this.tabInstalled.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.tabSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabHome;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TabPage tabAddons;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.Button buttonKspDir;
        private System.Windows.Forms.TextBox textBoxKspDir;
        private System.Windows.Forms.Label labelKspDir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabInstalled;
        private System.Windows.Forms.Label labelInstalled;
        private System.Windows.Forms.ListBox listBoxInstalled;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonInformation;
        private System.Windows.Forms.Button buttonBackup;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ListBox listBoxAddons;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBoxInformation;
        private System.Windows.Forms.Label labelCategory;
        private System.Windows.Forms.Label buttonForCategory;
        private System.Windows.Forms.Label labelAuthor;
        private System.Windows.Forms.Label labelForAuthor;
        private System.Windows.Forms.Label labelManu;
        private System.Windows.Forms.Label labelForManu;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelForDescription;
        private System.Windows.Forms.Label labelForName;
        private System.Windows.Forms.Button buttonInstall;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelSettingsVersion;
        private System.Windows.Forms.RichTextBox richTextBoxDesc;

    }
}

